import { Component, OnInit } from '@angular/core';
import { Camion } from 'src/app/_model/Camion';
import { CamionService } from 'src/app/_service/camion.service';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-camion',
  templateUrl: './camion.component.html',
  styleUrls: ['./camion.component.css']
})
export class CamionComponent implements OnInit {

  displayedColumns: string[] = ['tipo', 'cantidad', 'precio', 'guia','cliente','bodega','placa','fechaRegistro','fechaEntrega','accion'];
  
  dataSource = new MatTableDataSource<Camion>();
  camiones: Camion[] = [];

  constructor(
    private camionService:CamionService
  ) { }

  ngOnInit(): void {
    this.camionService.findAll2().subscribe(v=>{
      this.dataSource = new MatTableDataSource(v);
    })
  
  }

}
