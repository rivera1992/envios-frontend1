import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/_model/Cliente';
import { MaritimoService } from 'src/app/_service/maritimo.service';
import {MatTableDataSource} from '@angular/material/table';
import { Maritimo } from 'src/app/_model/Maritimo';

@Component({
  selector: 'app-maritimo',
  templateUrl: './maritimo.component.html',
  styleUrls: ['./maritimo.component.css']
})
export class MaritimoComponent implements OnInit {

  displayedColumns: string[] = ['tipo', 'cantidad', 'precio', 'guia','cliente','puertoEntrega','numeroFlota','fechaRegistro','fechaEntrega','accion'];
  
  dataSource = new MatTableDataSource<Maritimo>();
  camiones: Cliente[] = [];

  constructor(
    private maritimoService:MaritimoService
  ) { }

  ngOnInit(): void {
    this.maritimoService.findAll2().subscribe(v=>{
      this.dataSource = new MatTableDataSource(v);
    })
  
  }
}
