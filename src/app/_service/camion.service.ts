import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Camion } from '../_model/Camion';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class CamionService  {

  constructor(private http:HttpClient) {
  }


  findAll2(){
    return this.http.get<Camion[]>(environment.BASE_URI+'/api/camion');
  }

}
