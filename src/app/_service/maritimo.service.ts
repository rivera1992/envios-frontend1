import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Maritimo } from '../_model/Maritimo';
import { GenericService } from './generic.service';

@Injectable({
  providedIn: 'root'
})
export class MaritimoService  {

  constructor(private http:HttpClient) {
  }


  findAll2(){
    return this.http.get<Maritimo[]>(environment.BASE_URI+'/api/maritimo');
  }
}
