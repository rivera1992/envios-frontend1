import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class InterceptorInterceptor implements HttpInterceptor {

  constructor(
){

}

intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
    const token= "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtZWx2aW4ycm1AZ21haWwuY29tIiwiZXhwIjoxNjUwMjk3NjM3LCJyb2xlIjoiQURNSU4iLCJuYW1lIjoibWVsdmluIHJlbmUiLCJ1c2VySWQiOjEsImF1dGhvcml0aWVzIjpbIlJPTEVfQURNSU4iXX0.fI8mQsgw0b8oyy8GHfbLQKOytnKMy9wVKYw1cphzFCMir1DY70P0QARKSZ3a0bFaS689h_pcCYME-MR-lUgDzg";
    if(token){
        const cloned = req.clone({
            headers : req.headers.set('Authorization', `Bearer ${token}`)
        })
        return next.handle(cloned);
    }else{
        return next.handle(req);
    }
};
}
