import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CamionComponent } from './pages/camion/camion.component';
import { MaritimoComponent } from './pages/maritimo/maritimo.component';


const routes: Routes = [
  {
    path: 'camion',
    component: CamionComponent
  },
  {
    path: 'maritimo',
    component: MaritimoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
