import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material/material.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CamionComponent } from './pages/camion/camion.component';
import { MaritimoComponent } from './pages/maritimo/maritimo.component';
import { InterceptorInterceptor } from './helper/interceptor.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    CamionComponent,
    MaritimoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CommonModule,
    HttpClientModule ,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [//todos los interceptor deben estar en provaider
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorInterceptor, multi: true},
  ],  
  bootstrap: [AppComponent]
})
export class AppModule { }
