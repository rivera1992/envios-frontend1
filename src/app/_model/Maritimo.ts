import { Cliente } from "./Cliente";

export interface Maritimo {
    id: number;
    tipoProducto: any;
    cantidadProducto: number;
    fechaRegistro: Date;
    fechaEntrega: Date;
    precioEnvio: any;
    numeroGuia: string;
    cliente: Cliente;
    puertoEntrega: string;
    numeroFlota: string;
}