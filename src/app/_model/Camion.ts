import { Cliente } from "./Cliente";

export interface Camion {
    id: number;
    tipoProducto: string;
    cantidadProducto: number;
    fechaRegistro?: Date;
    fechaEntrega: Date;
    precioEnvio: number;
    numeroGuia: string;
    cliente: Cliente;
    bodegaEntrega: string;
    numeroPlaca: string;
}